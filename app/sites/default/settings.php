<?php

// phpcs:ignoreFile

if (file_exists($app_root . '/' . $site_path . '/../../../config/drupal/default/settings.local.php')) {
  include $app_root . '/' . $site_path . '/../../../config/drupal/default/settings.local.php';
}

// Automatically generated include for settings managed by ddev.
$ddev_settings = dirname(__FILE__) . '/settings.ddev.php';
if (getenv('IS_DDEV_PROJECT') == 'true' && is_readable($ddev_settings)) {
  require $ddev_settings;
}

// Include settings required for Redis cache.
if ((file_exists(__DIR__ . '/settings.ddev.redis.php') && getenv('IS_DDEV_PROJECT') == 'true')) {
  include __DIR__ . '/settings.ddev.redis.php';
}
